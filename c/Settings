/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******	Settings.c ********************************************************\

Project:	Ursula (RISC OS for Risc PC II)
Component:	Windows configuration plug-in
This file:	GUI <-> settings routines

History:
Date		Who	Change
----------------------------------------------------------------------------
09/12/1997	BJGA	Split from Main
			Added these headers
15/01/1998	BJGA	Adapted to use stored CMOS defaults
11/05/1998	BJGA	Now uses reorganised Wimp CMOS allocation
			Support for new Wimp options added (including iconbar values
			lookup table code and split display field / number range support)
29/06/1998	BJGA	Added support for tool button type optionbutton

\**************************************************************************/

/* CLib */
#include "swis.h"
/* Toolbox */
#include "toolbox.h"
#include "window.h"
#include "gadgets.h"
/* Common */
#include "cmos.h"
#include "misc.h"
/* local headers */
#include "Main.h"
#include "Settings.h"  /* includes prototypes for this file */

const cmos cmos_details [19] ={ { 0xDE, 7, 1 },		/* IconiseButton */
				{ 0x1C, 4, 1 },		/* WimpShiftToggle */
				{ 0xC5, 0, 1 },		/* WimpFlagsInstantDragMove */
				{ 0xC5, 1, 1 },		/* WimpFlagsInstantDragResize */
				{ 0xC5, 2, 1 },		/* WimpFlagsInstantDragHScroll */
				{ 0xC5, 3, 1 },		/* WimpFlagsInstantDragVScroll */
				{ 0xC5, 5, 1 },		/* WimpFlagsConfinementBR */
				{ 0xC5, 6, 1 },		/* WimpFlagsConfinementTL */
				{ 0x17, 5, 3 },		/* (IconBarSpeed index) EOR 4 */
				{ 0x1B, 5, 3 },		/* (IconBarAcceleration index) EOR 3 */
				{ 0x8C, 6, 1 },		/* ClickReleaseButtons */

				{ 0xC5, 7, 1 },		/* WimpFlagsSubMenus */
				{ 0x17, 0, 4 },		/* (WimpAutoMenuDelay EOR 10) */
				{ 0x17, 4, 1 },		/* WimpAutoMenuDelayUnit */
				{ 0x1B, 0, 4 },		/* (WimpMenuDragDelay EOR 10) */
				{ 0x1B, 4, 1 },		/* WimpMenuDragDelayUnit */

				{ 0x16, 7, 1 },		/* WimpAutoFrontIconBar */
				{ 0xDF, 4, 4 },		/* (WimpAutoFrontDelay EOR 5) */
				{ 0x16, 1, 1 } };	/* WimpAutoFrontDelayUnit */

/******	settings_read() ***************************************************\

Purpose:	Reads current / default settings, reflect them in GUI
In:		Routine to determine settings (cmos_read or cmos_default)

\**************************************************************************/

void settings_read (int(*get)(cmos item, void *messages))
{
  int value;
//
  throw (optionbutton_set_state (0, mainwindow_id, mainwindow_dragmove, get (WimpFlagsInstantDragMove, &messages)));
  throw (optionbutton_set_state (0, mainwindow_id, mainwindow_dragresize, get (WimpFlagsInstantDragResize, &messages)));
  throw (optionbutton_set_state (0, mainwindow_id, mainwindow_draghscroll, get (WimpFlagsInstantDragHScroll, &messages)));
  throw (optionbutton_set_state (0, mainwindow_id, mainwindow_dragvscroll, get (WimpFlagsInstantDragVScroll, &messages)));
//
  throw (optionbutton_set_state (0, mainwindow_id, mainwindow_bandr, get (WimpFlagsConfinementBR, &messages)));
  throw (optionbutton_set_state (0, mainwindow_id, mainwindow_tandl, get (WimpFlagsConfinementTL, &messages)));
  throw (optionbutton_set_state (0, mainwindow_id, mainwindow_clickrelease, !get (ClickReleaseButtons, &messages)));
  throw (optionbutton_set_state (0, mainwindow_id, mainwindow_iconise, get (IconiseButton, &messages)));
//
  throw (optionbutton_set_state (0, mainwindow_id, mainwindow_submenuauto, value = get (WimpFlagsSubMenus, &messages)));
  misc_shadecomponents (!value, mainwindow_id, mainwindow_submenuauto_shademin, mainwindow_submenuauto_shademax);
  value = get (WimpAutoMenuDelay, &messages) ^ 10;
  if (1 == get (WimpAutoMenuDelayUnit, &messages)) value *= 10;
  throw (numberrange_set_value (0, mainwindow_id, mainwindow_submenuopen, value));
  value = get (WimpMenuDragDelay, &messages) ^ 10;
  if (1 == get (WimpMenuDragDelayUnit, &messages)) value *= 10;
  throw (numberrange_set_value (0, mainwindow_id, mainwindow_submenuclose, value));
//
  value = get (IconBarSpeed, &messages) ^ 4;
  throw (numberrange_set_value (0, mainwindow_id, mainwindow_speed, value));
  settings_update_display (mainwindow_id, mainwindow_speed2, value);
  value = get (IconBarAcceleration, &messages) ^ 3;
  throw (numberrange_set_value (0, mainwindow_id, mainwindow_accel, value));
  settings_update_display (mainwindow_id, mainwindow_accel2, value);
//
  throw (optionbutton_set_state (0, mainwindow_id, mainwindow_shifttoggle, !get (WimpShiftToggle, &messages)));
//
  throw (optionbutton_set_state (0, mainwindow_id, mainwindow_iconbarfwd, value = !get (WimpAutoFrontIconBar, &messages)));
  misc_shadecomponents (!value, mainwindow_id, mainwindow_iconbarfwd_shademin, mainwindow_iconbarfwd_shademax);
  value = get (WimpAutoFrontDelay, &messages) ^ 5;
  if (1 == get (WimpAutoFrontDelayUnit, &messages)) value *= 10;
  throw (numberrange_set_value (0, mainwindow_id, mainwindow_iconbardelay, value));
//
}

/******	settings_write() **************************************************\

Purpose:	Reads GUI, reflect in current and configured settings
Out:		TRUE => operation performed successfully

\**************************************************************************/

BOOL settings_write (void)
{
  int value, ovalue;
  char buffer [6], string [60];
  BOOL reassert_mode = FALSE;
//
  throw (optionbutton_get_state (0, mainwindow_id, mainwindow_dragmove, &value));
  cmos_write (WimpFlagsInstantDragMove, value);
  throw (optionbutton_get_state (0, mainwindow_id, mainwindow_dragresize, &value));
  cmos_write (WimpFlagsInstantDragResize, value);
  throw (optionbutton_get_state (0, mainwindow_id, mainwindow_draghscroll, &value));
  cmos_write (WimpFlagsInstantDragHScroll, value);
  throw (optionbutton_get_state (0, mainwindow_id, mainwindow_dragvscroll, &value));
  cmos_write (WimpFlagsInstantDragVScroll, value);
//
  throw (optionbutton_get_state (0, mainwindow_id, mainwindow_bandr, &value));
  cmos_write (WimpFlagsConfinementBR, value);
  throw (optionbutton_get_state (0, mainwindow_id, mainwindow_tandl, &value));
  cmos_write (WimpFlagsConfinementTL, value);
  throw (optionbutton_get_state (0, mainwindow_id, mainwindow_clickrelease, &value));
  sprintf (string, "Configure WimpButtonType %s", 0 == value ? "Release" : "Click");
  _swi (OS_CLI, _IN(0), string);
  ovalue = cmos_read (IconiseButton, NULL);
  throw (optionbutton_get_state (0, mainwindow_id, mainwindow_iconise, &value));
  sprintf (string, "Configure WimpIconiseButton %s", 0 == value ? "Off" : "On");
  _swi (OS_CLI, _IN(0), string);
  if (value != ovalue) reassert_mode = TRUE;
//
  throw (optionbutton_get_state (0, mainwindow_id, mainwindow_submenuauto, &value));
  if (value) {
    cmos_write (WimpFlagsSubMenus, TRUE);
    throw (numberrange_get_value (0, mainwindow_id, mainwindow_submenuopen, &value));
    sprintf (string, "Configure WimpAutoMenuDelay %i", value);
    _swi (OS_CLI, _IN(0), string);
    throw (numberrange_get_value (0, mainwindow_id, mainwindow_submenuclose, &value));
    sprintf (string, "Configure WimpMenuDragDelay %i", value);
    _swi (OS_CLI, _IN(0), string);
  }
  else {
    cmos_write (WimpFlagsSubMenus, FALSE);
  }
//
  throw (displayfield_get_value (0, mainwindow_id, mainwindow_speed2, buffer, sizeof(buffer), NULL));
  sprintf (string, "Configure WimpIconBarSpeed %s", buffer);
  _swi (OS_CLI, _IN(0), string);
  throw (displayfield_get_value (0, mainwindow_id, mainwindow_accel2, buffer, sizeof(buffer), NULL));
  sprintf (string, "Configure WimpIconBarAcceleration %s", buffer);
  _swi (OS_CLI, _IN(0), string);
//
  throw (optionbutton_get_state (0, mainwindow_id, mainwindow_shifttoggle, &value));
  cmos_write (WimpShiftToggle, !value);
//
  throw (optionbutton_get_state (0, mainwindow_id, mainwindow_iconbarfwd, &value));
  if (value) {
    _swi (OS_CLI, _IN(0), "Configure WimpAutoFrontIconBar On");
    throw (numberrange_get_value (0, mainwindow_id, mainwindow_iconbardelay, &value));
    sprintf (string, "Configure WimpAutoFrontDelay %i", value);
    _swi (OS_CLI, _IN(0), string);
  }
  else {
    _swi (OS_CLI, _IN(0), "Configure WimpAutoFrontIconBar Off");
  }
//
  misc_applywimpflags ();
  if (reassert_mode) _swi (Wimp_SetMode, _IN(0), _swi (OS_ScreenMode, _IN(0)|_RETURN(1), 1));
  return TRUE;
}

/******	settings_update_display() *****************************************\

Purpose:	Reflects internal icon bar speed/accel number in display field
In:		Gadget paramaters, internal (log) value

\**************************************************************************/

void settings_update_display (ObjectId wind, ComponentId comp, int setting)
{
  int value;
  char string [6];
//
  switch (setting)
  {
    case 0: value = 0; break;
    case 1: value = 20; break;
    case 2: value = 50; break;
    case 3: value = 100; break;
    case 4: value = 200; break;
    case 5: value = 500; break;
    case 6: value = 1000; break;
    case 7: value = 2000; break;
    default: value = -1; break; /* shouldn't happen */
  }
  sprintf (string, "%d", value);
  throw (displayfield_set_value (0, wind, comp, string));
}
